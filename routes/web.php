<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/product', 'ProductController@index')->name('product');
Route::get('/search-product', 'ProductController@searchProduct')->name('searchProduct');
Route::post('/update-product', 'ProductController@updateProduct')->name('updateProduct');

//Route::post('import-file', 'ExcelController@importFile')->name('import.file');
Route::get('export', 'ProductController@exportView')->name('export');
Route::post('import', 'ProductController@import')->name('import');
Route::get('products-export', 'ProductController@productsExport')->name('products.export');

Route::get('/profile', 'UserController@profile')->name('profile');
Route::post('/update-profile', 'UserController@updateProfile')->name('update.profile');
Route::get('products-export', 'ProductController@productsExport')->name('products.export');