<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SearchProduct extends Model
{
    protected $table = 'mst_product';
    protected $primaryKey = 'product_code';
    public $incrementing = false;
    public $timestamps = false;

    public function getProduct(Request $request){
        $idAdmin = Auth::id();
        $query = $this->query();
        $query->where('price_supplier_id', '=', $idAdmin);

        // var get request
        $arr_cheetah_status = array();
        if ($request->has('販売')) {
            array_push($arr_cheetah_status, $request->廃盤);
        }
        if ($request->has('欠品')) {
            array_push($arr_cheetah_status, $request->欠品);
        }
        if ($request->has('販売')) {
            array_push($arr_cheetah_status, $request->販売);
        }

        //check query ステータス
        if (count($arr_cheetah_status) > 0) {
            $query->whereIn('cheetah_status', $arr_cheetah_status);
        }

        //check query product_jan
        if ($request->has('product_jan')&& $request->product_jan!= '') {
            //tach gia tri tu chuoi ra mang
            $arr_query_product_jan = explode(" ", trim($request->product_jan));
            $query->whereIn('product_jan', $arr_query_product_jan);
        }

        //check query maker_full_nm
        if ($request->has('maker_full_nm') && $request->maker_full_nm != '') {
            $query_product_maker_full_nm = explode(" ", $request->maker_full_nm);
            $query->whereIn('maker_full_nm', $query_product_maker_full_nm);
        }

        //check query m_product_code
        if ($request->has('product_code') && $request->product_code != '') {
            //tach gia tri tu chuoi ra mang
            $arr_query_m_product_code = explode(" ", trim($request->product_code));
            $query->whereIn('product_code', $arr_query_m_product_code);
        }

        //check query m_product_name
        if ($request->has('product_name') && $request->product_name != '') {
            $query_m_product_name = preg_replace("/\s+/", '|', trim($request->product_name));
            $query->where('product_name', 'REGEXP', $query_m_product_name);
        }
        //$query->select('cheetah_status', 'maker_full_nm', 'product_code', 'product_jan', 'product_name', 'list_price', 'process_status');
        return $query->select('cheetah_status', 'maker_full_nm', 'product_code', 'product_jan', 'product_name', 'list_price', 'process_status');

    }
    
}
