<?php

namespace App\Exports;

use App\Product;
use App\SearchProduct;
use Illuminate\Support\Facades\Request;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;


class ProductsQueryExport implements FromQuery,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    public function getRequest($request)
    {
        
        $this->request = $request;
        

        return $this;
    }

    public function query(){
        $products = new SearchProduct;
        $items = $products->getProduct($this->request);
        return $items;
        
    }

    public function headings(): array
    {
        return [
            'ステータス',
            'メーカー名',
            '品番',
            'JANコード',
            '商品名',
            '定価',
            '処理ステータス'
        ];
    }
}
