<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Image;

class UserController extends Controller
{
    
    public function profile(){
        $user=User::where('t_admin_id',Auth::id())->first();
        return view('user.profile')->with('user',$user);
    }

    public function updateProfile(Request $request){

        $user=Auth::user();

        $user->update($request->all());

        if($request->hasFile('avatar')){
            $avatar=$request->file('avatar');
            $fileName=time().'.'.$avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300,300)->save(public_path('upload/user-avatar/'.$fileName));
            $user->avatar=$fileName;
            $user->save();
        }
        
        return view('user.profile')->with('user', Auth::user());
    }
}
