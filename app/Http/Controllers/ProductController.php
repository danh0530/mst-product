<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\SearchProduct;
use Illuminate\Support\Facades\Auth;
use Storage;
use File;
use Log;
use Illuminate\Support\Facades\Input;
use App\Exports\ProductsQueryExport;
use Maatwebsite\Excel\Facades\Excel;


class ProductController extends Controller
{

    public function index(Request $request)
    {
        
        $products = new SearchProduct;
        $result = $products->getProduct($request)->paginate(50);
        return view('product.list')->with('products', $result);
    }

    public function searchProduct(Request $request)
    {
        $products = new SearchProduct;
        $result = $products->getProduct($request)->paginate(5)->setPath('');
        $pagination = $result->appends ( array (
            'product_code' => Input::get ( 'product_code' ) ,
            'product_jan' => Input::get ( 'product_jan' ) ,
            'product_name' => Input::get ( 'product_name' ) ,
            'maker_full_nm' => Input::get ( 'maker_full_nm' ) ,
            '販売' => Input::get ( '販売' ) ,
            '欠品' => Input::get ( '欠品' ) ,
            '廃盤' => Input::get ( '廃盤' ) ,
          ) );
        return view('product.search')->with('products',$result);
    }
    public function updateProduct( Request $request){

        $productCodeArray=explode(',',$request->product_code);
        $condition=$request->condition;
        
        if($condition==='01'){
            DB::table('mst_product')->whereIn('product_code', $productCodeArray)
            ->update(['cheetah_status'=>0,'process_status'=>1]);
        }
        else if( $condition === '11'){
            DB::table('mst_product')->whereIn('product_code', $productCodeArray)
                ->update(['cheetah_status' => 1, 'process_status' => 1]);
        }
        else{
            DB::table('mst_product')->whereIn('product_code', $productCodeArray)
                ->update(['cheetah_status' => 2, 'process_status' => 1]);
        }
        return redirect()->route('product');
    }

    public function import(Request $request) 
    {
        try {

            $file = $request->file_import;

            $type_file = $file->getClientOriginalExtension();

            $idAdmin = Auth::id();

            $arr_type = array('csv');
            if (!in_array($type_file, $arr_type)) {
                return 'This file must be a type .csv';
            }
            Storage::disk('local')->put('data.csv', File::get($file));
            $temp = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . 'data.csv';
            $temp = str_replace("\\", '/', $temp);

            DB::beginTransaction();

            // query import data
            $query = "LOAD DATA LOCAL INFILE '" . $temp . "'
            INTO TABLE mst_product CHARACTER SET
            UTF8 FIELDS TERMINATED BY ','
            OPTIONALLY ENCLOSED BY '\"'
            LINES TERMINATED BY '\r\n' IGNORE 1 LINES (cheetah_status, maker_full_nm, product_code,
            product_jan, product_name, list_price, process_status) SET price_supplier_id = " . $idAdmin;

            $result = DB::connection()->getpdo()->exec($query);
            if ($result) {
                DB::commit();
                Storage::delete('data.csv');
                return 'complete';
            }
        } catch (\Exception $e) {
            DB::rollback();
            echo $e;
        }
    }

    public function exportView(){
        return Excel::download(new ProductsExportView(),'products.csv');
    }

    public function productsExport(Request $request){
        try{
            return (new ProductsQueryExport)->getRequest($request)->download('products.csv');
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        
    }

}
