<?php

namespace App\Imports;

use App\Product;
use Maatwebsite\Excel\Concerns\ToModel;

class ProductsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Product([
            'product_code'=>$row['ステータス'],
            'product_name'=>$row['メーカー名'],
            'price_supplier_id'=>$row['品番'],
            'product_jan'=>$row['JANコード'],
            'maker_cd'=>$row['4'],
        ]);
    }
}
