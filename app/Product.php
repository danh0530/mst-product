<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table='mst_product';
    protected $fillable = ['product_code','product_name','cheetah_status','product_jan','process_status'];
    protected $primaryKey = 'product_code';   
    public $timestamps = false;
}
