@extends('home')
@section('content')
    <div class="content mt-3">
            <div class="animated fadeIn">

                <div class="row">
                  <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">User Profile</strong>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  
                                  <form action="/update-profile" method="post" enctype="multipart/form-data">
                                      {{ csrf_field() }}
                                      <div class="form-group">
                                          <label for="name">Your name</label>
                                      <input id="name" name="user_name" type="text" class="form-control" aria-required="true" aria-invalid="false" value="{{$user->user_name}}">
                                      </div>
                                      <div class="form-group has-success">
                                          <label for="name">Your email</label>
                                      <input id="email" name="email" value="{{$user->email}}" type="text" class="form-control email valid" >
                                          <span class="help-block field-validation-valid" data-valmsg-for="email" ></span>
                                      </div>
                                      
                                      <div class="row">
                                          <div class="col-md-6">
                                            <img src="/upload/user-avatar/{{Auth::user()->avatar}}" alt="">
                                          </div>
                                          <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="avatar">Choose new avatar</label>
                                                <input id="avatar" name="avatar" type="file" class="form-control">
                                            </div>
                                          </div>
                                      </div>
                                      
                                      <div>
                                          <input type="submit" value="Update" class="btn btn-lg btn-info btn-block">
                                              
                                          
                                      </div>
                                  </form>
                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->

                  
                </div>


            </div><!-- .animated -->
        </div>
@endsection
