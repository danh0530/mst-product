<table class="table">
        <thead>
        <tr class="bg-primary text-white">
            <th>ステータス</th>
            <th>メーカー名</th>
            <th>品番</th>
            <th>JANコード</th>
            <th>商品名</th>
            <th>定価</th>
            <th>処理ステータス</th>
            <th>出荷指示
                +全て選択
                -選択解除
            </th>
        </tr>
        </thead>
        <tbody>
            @foreach ($products as $product)
                <tr>
                    <td>
                        @if ($product->cheetah_status==0)
                            <span class="badge badge-primary">販売</span>
                        @elseif($product->cheetah_status==0)
                            <span class="badge badge-info">欠品</span>
                        @else 
                            <span class="badge badge-warning">廃盤</span>
                        @endif
                    </td>
                    <td>
                        {{$product->maker_full_nm}}
                    </td>
                    <td>
                        {{$product->product_code}}
                    </td>
                    <td>
                        {{$product->product_jan}}
                    </td>
                    <td>
                        {{$product->product_name}}
                    </td>
                    <td>
                        {{$product->list_price}}
                    </td>
                    <td>
                        @if ($product->process_status==0)
                            <span class="badge badge-primary">Default</span>
                        @elseif($product->process_status==1)
                            <span class="badge badge-info">Waiting</span>
                        @else 
                            <span class="badge badge-warning">Approved</span>
                        @endif
                        
                    </td>
                    <td>
                    <input type="checkbox" name="choose[]" id="choose" value="{{$product->product_code}}">
                    </td>
                </tr>
            @endforeach
        
        
        </tbody>
    </table>