@extends('home')
@section('content')
    <div class="card">
        
        <div class="card-body">

            <div class="row form-search">
                <div class="col">
                    <h3>廃盤/欠品/販売</h3>
                        <br>
                    <form class="form-inline" id="searchForm" action="" method="get">
                        {{ csrf_field() }}
                        
                        <div class="row form-group mt-3">
                                <div class="col ">
                                    
                                    <div class="form-check-inline form-check">
                                        <label for="inline-checkbox1" class="form-check-label ">
                                        <input type="checkbox" name="販売" value="2"
                                            @isset($_GET['販売']) checked @endisset class="form-check-input">廃番 
                                        </label>
                                        <label for="inline-checkbox2" class="form-check-label ">
                                        <input type="checkbox" name="欠品" value="1" 
                                            @isset($_GET['欠品']) checked @endisset class="form-check-input"> 欠品 
                                        </label>
                                        <label for="inline-checkbox3" class="form-check-label ">
                                        <input type="checkbox" name="廃盤" value="0" 
                                            @isset($_GET['廃盤']) checked @endisset class="form-check-input">販売
                                        </label>
                                    </div>
                                </div>
                            </div>
                        <div class="form-group mt-3">
                            <label for="">JANコード</label>
                        <input type="text" class="form-control" value=""  name="product_jan">
                        </div>
                        <div class="form-group mt-3">
                            <label for="pwd">メーカー名</label>
                        <input type="text" class="form-control" value=""  name="maker_full_nm">
                        </div>
                        <div class="form-group mt-3">
                            <label for="pwd">品番</label>
                            <input type="text" class="form-control" value="" name="product_code">
                        </div>
                        <div class="form-group mt-3">
                            <label for="pwd">商品名</label>
                            <input type="text" class="form-control" value="" name="product_name">
                        </div>
                        
                        <input type="submit" value="検索する" class="btn btn-primary mt-3 ml-2">
                        <a  href="/product"  class="btn btn-primary ml-2 mt-3">元に戻す</a>
                    </form>
                </div>
            </div>

            <div class="change_status mt-3 text-right">
                <button class="btn btn-danger" type="submit" id="button21" data-toggle="modal" data-target="#cheetahStatus">
                廃番
                </button>
                <button class="btn btn-warning" type="submit" id="button11" data-toggle="modal" data-target="#cheetahStatus">
                欠品にする
                </button>
                <button class="btn btn-info" type="submit" id="button01" data-toggle="modal" data-target="#cheetahStatus">
                販売にする
                </button>
            </div>

            <div class="product-table">
                
                <h2>Result</h2>
                <div class="d-flex justify-content-between import-export">
                        
                        <button class="btn btn-danger" id="exportData">Export data</button>
                        
                        <form class="form-inline" action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="file" name="file" class="form-control">
                            </div>
                            <button class="btn btn-success" type="submit">Import User Data</button>
                            
                        </form>    
                        
                        
                    </div>        
                @include('product.table',$products)
            </div>


            
        </div>
        <div class="card-footer">
            {{$products->links()}}
        </div>
    </div>
    

<!-- Modal for cheetah status-->
        <div class="modal fade" id="cheetahStatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
                <p><button class="btn btn-primary">廃番</button> 下記の商品を、 廃番 にしても良い場合は、 確定 ボタンを押してください。</p>
                <p><button class="btn btn-warning">廃番</button> 下記の商品を、 廃番 にしても良い場合は、 確定 ボタンを押してください。</p>
                <p>
                    <button class="btn btn-outline-primary">Yamaha-enhpp</button>
                    <button class="btn btn-outline-primary">Yamaha-enhpp</button>
                    <button class="btn btn-outline-primary">Yamaha-emfa6</button>
                    <button class="btn btn-outline-primary">Yamaha-emb</button>
                    <button class="btn btn-outline-primary">Y03_0008_1</button>
                </p>
            </div>
            <div class="modal-footer">
                <form action="/update-product" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="product_code" id="productCode" value="">
                    <input type="hidden" name="condition" id="condition" value="">
                    <button type="button" class="btn btn-warning" data-dismiss="modal"> 閉じる</button>
                    <button type="submit" id="updateProduct" class="btn btn-primary">確定</button>
                </form>
                
            </div>
            </div>
        </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function(){
                
                $('#exportData').click(function() {
                    let dataForm = $('#searchForm').serialize();
                    document.location.href = '{{ route('products.export') }}?' + dataForm;
                });
            })
        </script>
@endsection